/* 1. Write a JavaScript function that reverse a number. 
Example x = 32243;
Expected Output : 34223 */

function reverseNumber (num) {
	num = num + "";
	return num.split("").reverse().join("");
}
console.log(reverseNumber(34223));